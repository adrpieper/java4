package exceptions;

import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-07.
 */
public class IFDivide {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwie liczby");
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        if (b!=0) {
            System.out.println(a+"/"+b+"="+a/b);
        }else {
            System.out.println("Nie można dzielić");
        }
    }
}
