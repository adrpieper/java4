package exceptions;

import java.util.Random;

/**
 * Created by Adrian on 2017-08-07.
 */
public class MyOwnExceptionTest {

    public static void main(String[] args) {
        try {
            throwException();
        } catch (MyOwnException e) {
            System.out.println("Błąd :(, spróbuj jeszcze raz.");
        }
    }

    private static void throwException() throws MyOwnException {
        Random random = new Random();
        if (random.nextBoolean()) {
            throw new MyOwnException();
        }
    }
}
