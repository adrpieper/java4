package exceptions;

/**
 * Created by Adrian on 2017-08-07.
 */
public class Example {

    public static void main(String[] args) {

        try {
            System.out.println(3/2);
            System.out.println(3/1);
            int[] t = {};
            System.out.println(t[0]);
        }catch (ArithmeticException e) {
            System.out.println("Nie można dzielić przez 0");
        }finally {
            System.out.println("finnally");
        }
        System.out.println("Za blokiem try");
    }
}
