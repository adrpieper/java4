package exceptions;

import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-07.
 */
public class TryCatchDivide {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwie liczby");
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        try {
            System.out.println(a+"/"+b+"="+a/b);
        }catch (ArithmeticException e) {
            System.out.println("Nie można dzielić");
        }
    }

    public int divide (int a, int b) {
        try {
            return a/b;
        }catch (ArithmeticException e) {
            return 0;
        }
    }
}
