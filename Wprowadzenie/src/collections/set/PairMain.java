package collections.set;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Adrian on 2017-08-10.
 */
public class PairMain {

    public static void main(String[] args) {
        Set<PairOfInt> set = new HashSet<>();
        set.add(new PairOfInt(1, 2));
        set.add(new PairOfInt(1, 2));
        set.add(new PairOfInt(2,1));
        set.add(new PairOfInt(1,1));
        System.out.println(set);

        PairOfInt p1 = new PairOfInt(1,2);
        PairOfInt p2 = new PairOfInt(1,2);
        System.out.println(p1.equals(p2));

    }
}
