package collections.set;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Adrian on 2017-08-10.
 */
public class SimpleSetExample {

    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        int[] numbers = {10,12,10,3,4,12,12,300,12,40,55};
        for (int number : numbers) {
            set.add(number);
        }

        System.out.println("Czy zawiera powtórzenia : " + (set.size() != numbers.length));
        System.out.println("size : " + set.size());
        System.out.println("Elementy zbioru");
        for (int number : set) {
            System.out.println(number);
        }
        set.remove(10);
        set.remove(12);
        System.out.println("size : " + set.size());
        System.out.println("Elementy zbioru");
        for (int number : set) {
            System.out.println(number);
        }

    }
}
