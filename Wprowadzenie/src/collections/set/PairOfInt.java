package collections.set;

/**
 * Created by Adrian on 2017-08-10.
 */
public class PairOfInt {
    private int a;
    private int b;

    public PairOfInt(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public String toString() {
        return "{"+a+","+b+"}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PairOfInt pairOfInt = (PairOfInt) o;

        return a == pairOfInt.a && b == pairOfInt.b;
    }

    @Override
    public int hashCode() {
        int result = a;
        result = 31 * result + b;
        return result;
    }
}
