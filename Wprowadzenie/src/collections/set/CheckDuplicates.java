package collections.set;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Adrian on 2017-08-10.
 */
public class CheckDuplicates {

    public static void main(String[] args) {

        System.out.println("abc :" + containDuplicates("abc") );
        System.out.println("aba :" + containDuplicates("aba") );
    }

    public static boolean containDuplicates(String text) {
        char[] chars = text.toCharArray();

        Set<Character> set = new HashSet<>();

        for (char c : chars) {
            set.add(c);
        }

        return set.size() != text.length();
    }
}
