package collections.queue;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Adrian on 2017-08-11.
 */
public class PersonQueuePriorityByName {

    public static void main(String[] args) {
        Queue<Person> peopleQueue = new PriorityQueue<>(new NameComparator());

        peopleQueue.offer(new Person("Jan", 20));
        peopleQueue.offer(new Person("Adam", 22));
        peopleQueue.offer(new Person("Janina", 18));
        peopleQueue.offer(new Person("Grażyna", 30));

        while (peopleQueue.size() > 0) {
            Person person = peopleQueue.poll();
            System.out.println(person.getName());
        }
    }

    static class NameComparator implements Comparator<Person> {

        @Override
        public int compare(Person o1, Person o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
