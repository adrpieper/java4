package collections.queue;

/**
 * Created by Adrian on 2017-08-11.
 */
public class Person implements Comparable<Person> {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public int compareTo(Person other) {
        //  Opcja 1
        //       if (this.age == other.age) {
        //            return 0;
        //        } else if (this.age > other.age) {
        //            return -1;
        //        }
        //        return 1;
        //  Opcja 2
        //  ther.age - this.age
        //Opcja 3
        return Integer.compare(other.age,this.age);
    }
}
