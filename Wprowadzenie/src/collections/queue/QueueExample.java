package collections.queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Adrian on 2017-08-11.
 */
public class QueueExample {

    public static void main(String[] args) {
        /*
        Elementy są wkładane (offer), a następnie wyciągane z kolejki (poll).
        O kolejności wyciągania definiuje klasa kolejki:
        PriorityQueue - od najmniejszej do najwięszej
        LinkedList - FIFO, czyli według kolejności umiejszczania w kolejce
         */

        Queue<Integer> priorityQueue = new PriorityQueue<>();
        priorityQueue.offer(1);
        priorityQueue.offer(21);
        priorityQueue.offer(2);
        priorityQueue.offer(10);
        priorityQueue.offer(12);

        while (priorityQueue.size() > 0) {
            System.out.println(priorityQueue.poll());
        }

        Queue<Integer> fifoQueue = new LinkedList<>();
        fifoQueue.offer(1);
        fifoQueue.offer(21);
        fifoQueue.offer(2);
        fifoQueue.offer(10);
        fifoQueue.offer(12);

        while (fifoQueue.size() > 0) {
            System.out.println(fifoQueue.poll());
        }

        Queue<Integer> queue = new LinkedList<>();
        queue.offer(1);
        queue.offer(2);
        System.out.println("peek (zwraca pierwszy element lub null) : " +queue.peek());
        System.out.println("element (zwraca pierwszy element lub wyjątek) : " + queue.element());
        System.out.println("pool (zwraca pierwszy i wyciąga) : " +queue.poll());
        System.out.println("pool (zwraca pierwszy i wyciąga) : " +queue.poll());
    }
}
