package collections.map;

/**
 * Created by Adrian on 2017-08-11.
 */
public class UniversityMain {

    public static void main(String[] args) {
        University university = new University();
        university.addStudent(100200,"Jan", "Kowalski");
        university.addStudent(100400,"Jan", "Kowalski");
        System.out.println(university.studentsCount());
        System.out.println(university.containsStudent(100200));
        System.out.println(university.getStudent(100400).getName());
        university.printAllStudent();
    }
}
