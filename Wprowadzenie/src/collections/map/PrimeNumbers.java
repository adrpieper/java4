package collections.map;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Adrian on 2017-08-10.
 */
public class PrimeNumbers {

    public static void main(String[] args) {
        Map<Integer,Boolean> map = new HashMap<>();
        map.put(1, false);
        map.put(2, true);
        map.put(3, true);
        map.put(4, false);
        map.put(5, true);
        map.put(6, false);
        map.put(7, true);
        map.put(8, false);
        map.put(9, false);
        map.put(10, false);
        map.put(11, true);
        map.put(12, false);
        map.put(13, true);
        map.put(14, false);
        map.put(15, false);
        map.put(16, false);
        map.put(17, true);
        map.put(18, false);
        map.put(19, true);
        map.put(20, false);
        map.put(21, false);
        map.put(22, false);
        map.put(23, true);
        map.put(24, false);
        map.put(25, false);
        map.put(26, false);
        map.put(27, false);
        map.put(28, false);
        map.put(29, true);
        map.put(30, false);

        for (Map.Entry<Integer, Boolean> entry : map.entrySet()) {

            System.out.println(entry.getKey() + " jest pierwsza : " + entry.getValue());
        }

        System.out.println("1 jest pierwsza : " + map.get(1));
        System.out.println("4 jest pierwsza : " + map.get(4));
        System.out.println("5 jest pierwsza : " + map.get(5));
        System.out.println("7 jest pierwsza : " + map.get(7));
        System.out.println("9 jest pierwsza : " + map.get(9));
        System.out.println("12 jest pierwsza : " + map.get(12));

    }
}
