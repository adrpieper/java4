package collections.map;

/**
 * Created by Adrian on 2017-08-11.
 */
public class Student {
    private long indexNumber;
    private String name;
    private String surname;

    public Student(long indexNumber, String name, String surname) {
        this.indexNumber = indexNumber;
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getIndexNumber() {
        return indexNumber;
    }
}
