package collections.map;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Adrian on 2017-08-10.
 */
public class MapExample {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("Jakub", 2);
        map.put("Adam", 4);
        map.put("Weronika", 5);
        map.put("Ewa", 1);
        System.out.println("Zbiór kluczy, wartości i encja :");
        System.out.println(map.keySet());
        System.out.println(map.values());
        System.out.println(map.entrySet());
        System.out.println("Size : " + map.size());
        System.out.println("Wartość klucza Jakub");
        System.out.println(map.get("Jakub"));
        System.out.println("Wartość klucza Kuba");
        System.out.println(map.get("Kuba"));
        System.out.println(map.getOrDefault("Kuba", 0));
        System.out.println("Contains dla klucza Jakub i Kuba");
        System.out.println(map.containsKey("Jakub"));
        System.out.println(map.containsKey("Kuba"));
        map.remove("Jakub");
        System.out.println("Contains dla klucza Jakub i Kuba po remove Jakub");
        System.out.println(map.containsKey("Jakub"));
        System.out.println(map.containsKey("Kuba"));
        map.put("Adam", 12);
        System.out.println("Zbiór encji po wykonaniu map.put(\"Adam\", 12)");
        System.out.println(map.entrySet());
        map.clear();
        System.out.println("Size po wykonaniu clear : " + map.size());


    }
}
