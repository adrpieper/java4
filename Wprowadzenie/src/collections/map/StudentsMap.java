package collections.map;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Adrian on 2017-08-11.
 */
public class StudentsMap {

    public static void main(String[] args) {
        Map<Long,Student> students = new HashMap<>();
        Student student100200 = new Student(100200,"Jan", "Kowalski");
        students.put(student100200.getIndexNumber(), student100200);
            Student student100400 = new Student(100400,"Jan", "Kowalski");
        students.put(student100400.getIndexNumber(), student100400);
        System.out.println("Czy istnieje student 100200 : " + students.containsKey(100200L));
        Student studentGet = students.get(100400L);
        print(studentGet);
        System.out.println("Liczba studentów : " + students.size());
        for (Student student : students.values()) {
            print(student);
        }

    }

    private static void print(Student student) {
        System.out.println(student.getIndexNumber() + " " + student.getName() + " " + student.getSurname());
    }
}
