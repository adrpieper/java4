package collections.map;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Adrian on 2017-08-11.
 */
public class University {
    private Map<Long,Student> students = new HashMap<>();

    public void addStudent(long indexNumber, String name, String surname) {
        Student student = new Student(indexNumber, name, surname);
        students.put(indexNumber, student);
    }

    public int studentsCount(){
        return students.size();
    }

    public boolean containsStudent(long indexNumber)  {
        return students.containsKey(indexNumber);
    }
    public Student getStudent(long indexNumber) {
        return students.get(indexNumber);
    }
    public void printAllStudent() {
        for (Student student : students.values()) {
            System.out.println(student.getIndexNumber() + " " + student.getName() + " " + student.getSurname());
        }
    }

}
