package collections.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Adrian on 2017-08-09.
 */
public class ListsMethods {

    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(2);
        numbers.add(2);
        numbers.add(7);
        System.out.println(sum(numbers));
        int [] array =  {4,2,2,1,5,29,3,8};
        List<Integer> list = Arrays.asList(1,2,4,2,5,12,3,2);
        System.out.printf("duplikaty : " + countDuplicates(list, array) );

        newArrayList(1,2,3,4);
    }

    public static int sum(List<Integer> numbers) {
        int result = 0;
        for (Integer number : numbers) {
            result += number;
        }
        return result;
    }

    public static int countDuplicates(List<Integer> list, int[] array) {
        int count = 0;
        for (int i = 0; i < array.length && i < list.size(); i++) {
            if (list.get(i) == array[i]) {
                count++;
            }
        }

        return count;
    }

    public static List<Integer> newArrayList(int... numbers) {
        List<Integer> list = new ArrayList<>();
        for (int i : numbers) {
            list.add(i);
        }
        return list;
    }
}
