package collections.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 2017-08-09.
 */
public class ListExamples {

    public static void main(String[] args) {
        List<String> wordsList = new ArrayList<>();
        wordsList.add("aa");
        wordsList.add("bb");
        wordsList.add("cc");

        System.out.println("GET");
        System.out.println(wordsList.get(0));
        System.out.println(wordsList.get(1));
        System.out.println(wordsList.get(2));

        System.out.println("ForEach");
        for (String word : wordsList) {
            System.out.println(word);
        }

        System.out.println("Size");
        System.out.println(wordsList.size());

        System.out.println("Remove");
        wordsList.remove(1);
        for (String word : wordsList) {
            System.out.println(word);
        }

        System.out.println("Contains");
        System.out.println(wordsList.contains("aa"));
        System.out.println(wordsList.contains("bb"));

        System.out.println("IsEmpty");
        System.out.println(wordsList.isEmpty());

        System.out.println("Clea");
        wordsList.clear();
        System.out.println("IsEmpty : " + wordsList.isEmpty());
        System.out.println("Size : " + wordsList.size());
        System.out.println("get(0) : " + wordsList.get(0));

    }
}
