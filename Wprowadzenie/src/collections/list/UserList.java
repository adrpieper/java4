package collections.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 2017-08-09.
 */
public class UserList {
    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        users.add(new User("Jan","1234"));
        users.add(new User("Adam","221"));
        print(users);
    }

    private static void print(List<User> users) {
        for (User user : users) {
            System.out.println(user);
        }
    }
}
