package loop;

import java.util.Scanner;

/**
 * Created by Adrian on 2017-07-28.
 */
public class DoWhileHelloWorld {

    public static void main(String[] args) {
        //Tu piszemy program
        Scanner scanner = new Scanner(System.in);
        String command;
        do {
            System.out.println("Hello World!!!");
            command = scanner.next();
        }while (!command.equals("koniec"));
    }
}
