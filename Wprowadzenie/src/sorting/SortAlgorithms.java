package sorting;

import java.util.Arrays;

/**
 * Created by Adrian on 2017-08-29.
 */
public class SortAlgorithms {

    public static void main(String[] args) {

        int[] numbers = {10, 4, 3, 6, 1, 2, 6, 1};
        bubbleSort(numbers);
        System.out.println(Arrays.toString(numbers));
    }


    public static void bubbleSort(int[] numbers) {
        for (int j = numbers.length - 1; j >= 0; j--) {
            for (int i = 0; i < j; i++) {
                if (numbers[i] > numbers[i + 1]) {
                    int buffor = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = buffor;
                }

            }
        }
    }
}

