package complexity;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Adrian on 2017-08-28.
 */
public class Examples {

    private static Random random = new Random();

    public static void main(String[] args) {
        System.out.println("1+2=" + sum(1, 2));
        System.out.println("1+2+3+4+5=" + sum(1, 2, 3, 4, 5));
        System.out.println("2^6=" + pow(2, 6));
        System.out.println("1+2+3+4+5+6+7+8+9=" + sum(1, 2, 3, 4, 5, 6, 7, 8, 9));

        for (int i : randArray(4)) {
            System.out.println(i);
        }

        for (int[] row : rand2Array(4)) {
            for (int i : row) {
                System.out.print(i);
                System.out.print(' ');
            }
            System.out.println();

        }

        int[] numbers1 = {1, 2, 3};
        int[] numbers2 = {1, 2, 3, 4};
        int[] result = concat(numbers1, numbers2); // {1,2,3,1,2,3,4}
        System.out.println(Arrays.toString(result));
        System.out.println("Aaaaa" + "bbbbb"); // Konkatenacja O(n)

        String text = "";
        for (int i = 0; i < 10; i++) {
            text += "a";
        }
        System.out.println(text);

        // chcę wygenerować {1,1,1,1,1,1,1,1,1,1}
        int[] array = {};
        for (int i = 0; i < 10; i++) {
            int[] one = {1};
            array = concat(array, one);
        }
        System.out.println(Arrays.toString(array));

        int[] numbers = {1, 2, 3, 14, 15, 16, 71, 121, 200, 210};
        System.out.println(indexOf(14, numbers));
        System.out.println(indexOf(140, numbers));
        System.out.println(fastSearch(14, numbers));
        System.out.println(fastSearch(210, numbers));
        System.out.println(fastSearch(1, numbers));

        int[] lotOfNumbers = new int[1024*1024];
        for (int i = 0; i < lotOfNumbers.length; i++) {
            lotOfNumbers[i] = i * 2;
        }

        Random random = new Random();
        LocalDateTime start,end;
        start = LocalDateTime.now();
        int repeats = 1;
        for (int i = 0; i < repeats; i++) {
            int number = random.nextInt(1024*1024);
            int index = indexOf(number, lotOfNumbers);
        }
        end = LocalDateTime.now();
        System.out.println("Slow : " + Duration.between(start,end).toMillis() + " ms");

        start = LocalDateTime.now();
        for (int i = 0; i < repeats; i++) {
            int number = random.nextInt(1024*1024);
            int index = fastSearch(number, lotOfNumbers);
        }
        end = LocalDateTime.now();
        System.out.println("Fast : " + Duration.between(start,end).toMillis() + " ms");
    }

    // Złożoność pesymistyczna - O(n)
    // Złożoność optymistyczna- O(1)
    // Złożoność średnia - O(n)
    private static int indexOf(int number, int[] numbersArray) {
        for (int i = 0; i < numbersArray.length; i++) {
            if (number == numbersArray[i]) {
                return i;
            }
        }
        return -1;
    }

    private static int fastSearch(int number, int[] numbersArray) {
        int min = 0;
        int max = numbersArray.length - 1;
        while (min <= max) {
            int middle = ( max + min ) / 2;
            if (numbersArray[middle] > number) {
                max = middle - 1;
            }else if (numbersArray[middle] < number) {
                min = middle + 1;
            }else {
                return middle;
            }
        }
        return -1;
    }

    public static int sum(int a, int b) { // O(1)
        return a + b;
    }

    public static int sum(int a, int b, int c, int d, int e) { // O(1)
        return a + b + c + d + e;
    }

    public static long pow(int a, int n) { // n + 1 O(n)
        long result = 1;
        for (int i = 0; i < n; i++) {
            result *= a;
        }
        return result;
    }

    public static int sum(int... numbers) {  // n + 1 O(n)
        int result = 0;
        for (int number : numbers) {
            result += number;
        }
        return result;
    }

    public static int[] randArray(int n) { // n + n = 2n O(n)
        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            result[i] = random.nextInt(10);
        }
        return result;
    }

    public static int[][] rand2Array(int n) { //  O(n^2)
        int[][] result = new int[n][]; // n - Tylko jedna tablica
        for (int i = 0; i < n; i++) {
            result[i] = randArray(n); //n
        }
        return result;
    }

    public static int[] concat(int[] left, int[] right) { // O(n)
        int[] result = new int[left.length + right.length];
        for (int i = 0; i < left.length; i++) {
            result[i] = left[i];
        }
        int offset = left.length;
        for (int i = 0; i < right.length; i++) {
            result[offset + i] = right[i];
        }
        return result;
    }


}
