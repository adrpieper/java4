package mutable_immutable;

/**
 * Created by Adrian on 2017-08-16.
 */
public class StringImmutable {

    public static void main(String[] args) {
        String imie = "jan";
        imie = imie.toUpperCase();
        System.out.println(imie);
    }
}
