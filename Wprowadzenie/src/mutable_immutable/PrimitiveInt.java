package mutable_immutable;

/**
 * Created by Adrian on 2017-08-16.
 */
public class PrimitiveInt {

    public static void main(String[] args) {
        int a = 6;
        int b = 5;
        System.out.println(a == b);
        System.out.println(a);
        System.out.println(b);
        a = b;
        System.out.println(a == b);
        b = 3;
        System.out.println(a == b);
        System.out.println(a);
        System.out.println(b);
    }
}
