package mutable_immutable;

/**
 * Created by Adrian on 2017-08-16.
 */
public class MutableIntegerExample {

    public static void main(String[] args) {
        MutableInteger a = new MutableInteger(160);
        MutableInteger b = new MutableInteger(160);
        System.out.println(a.equals(b));
        System.out.println(a == b);
        System.out.println(a);
        System.out.println(b);
        a = b;
        System.out.println(a.equals(b));
        System.out.println(a == b);
        b.setValue(3);
        System.out.println(a.equals(b));
        System.out.println(a == b);
        System.out.println(a);
        System.out.println(b);
        b.add(5);
        System.out.println(a);
        System.out.println(b);
    }
}
