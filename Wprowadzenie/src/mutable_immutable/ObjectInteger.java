package mutable_immutable;

/**
 * Created by Adrian on 2017-08-16.
 */
public class ObjectInteger {

    public static void main(String[] args) {
        Integer a = 160;
        Integer b = 160;
        System.out.println(a.equals(b));
        System.out.println(a == b);
        System.out.println(a);
        System.out.println(b);
        a = b;
        System.out.println(a.equals(b));
        System.out.println(a == b);
        b = 3;
        System.out.println(a.equals(b));
        System.out.println(a == b);
        System.out.println(a);
        System.out.println(b);
    }
}
