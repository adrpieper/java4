package overloading;

/**
 * Created by Adrian on 2017-08-01.
 */
public class MyMath {

    public static void main(String[] args) {
        int a = max(2,5);
        System.out.println(a);
        double b = max(2.0,5.0);
        System.out.println(b);

        System.out.println(abs(-6));
        System.out.println(abs(-2.5));
        System.out.println(pow(2,3));
        System.out.println(pow(2,-3.0));
    }

    public static long pow(int a, int b) {
        long result = 1;
        if (b<0) {
            throw new IllegalArgumentException("b is nagativ : " + b);
        }
        for (int i = 0; i < b; i++) {
            result *= a;
        }

        return result;
    }

    public static double pow(double a, double b) {
        double result = 1;
        if (b<0) {
            return 1/pow(a,-b);
        }
        for (int i = 0; i < b; i++) {
            result *= a;
        }

        return result;
    }

    public static int max(int a, int b) {
        if (a>b) {
            return a;
        }else {
            return b;
        }
    }

    public static double max(double a, double b) {
        if (a>b) {
            return a;
        }else {
            return b;
        }
    }

    public static int abs(int a) {
        if (a<0) {
            return -a;
        }else {
            return a;
        }
    }

    public static double abs(double a) {
        if (a<0) {
            return -a;
        }else {
            return a;
        }
    }
}
