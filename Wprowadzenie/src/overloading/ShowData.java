package overloading;

/**
 * Created by Adrian on 2017-08-01.
 */
public class ShowData {

    /*
    Mechanizm przeciążania metod - metody o tej samej nazwie mogą być
    umieszczone w jednej klasie jeżeli mają różne parametry (ilość lub typ)
     */

    public static void main(String[] args) {
        show(4);
        show(4.0);
        show("4");
        show('4');
    }

    public static void show(int number) {
        System.out.println("Liczba całkowita : " + number);
    }

    public static void show(double number) {
        System.out.println("Liczba rzeczywista : " + number);

    }

    public static void show(String text) {
        System.out.println("Tekst  : " + text);
    }


    public static void show(char c) {
        System.out.println("Znak  : " + c);
    }
}
