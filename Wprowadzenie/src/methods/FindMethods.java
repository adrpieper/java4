package methods;

/**
 * Created by Adrian on 2017-08-01.
 */
public class FindMethods {

    public static void main(String[] args) {
        PrintMethods.printNumberWithName("min(3,6)",min(3,6));
        PrintMethods.printNumberWithName("min(3,1)",min(3,1));
        PrintMethods.printNumberWithName("min2(3,1)",min2(3,1));
        PrintMethods.printNumberWithName("min(3,1,8)",min(3,1,8));
        PrintMethods.printNumberWithName("min({3,1,8})",min(new int[]{3,1,8}));
        PrintMethods.printNumberWithName("min({3,1,8})",min(new int[0]));
    }

    public static int min(int a, int b) {
        if (a>b) {
            return b;
        }
        else {
            return a;
        }
    }

    public static int min2(int a, int b) {
        return (a>b) ? b : a;
    }

    public static int min(int a, int b, int c) {
        return min(min(a,b), c);
    }

    public static int min(int[] numbers) {
        int result = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            result = min(result, numbers[i]);
        }

        return result;
    }
}
