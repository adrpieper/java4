package methods;

/**
 * Created by Adrian on 2017-07-31.
 */
public class PrintMethods {

    public static void main(String[] args) {
        printNumber(3);
        printNumber(5);
        printNumber(8);
        printNumberWithName("a",6); // "a = 6"
        printNumberWithName("długość",10); // "długość = 10"
        printSum(4,6); // "10" * "4+6=10"
        double [] numbers = {1,2,3.5};
        printArray(numbers);
        printArray(new double[]{1.25,6,8});
        checkIfTrue(4 < 8);
        checkIfTrue(true);
        checkIfTrue(!true);
    }

    public static void printNumber(int value) {
        System.out.println("Liczba = " + value);
    }

    public static void printNumberWithName(String name, int number) {
        System.out.println(name + " = " + number);
    }

    public static void printSum(int a, int b) {
        System.out.println(a + "+" + b + " = " + (a+b));
    }

    public static void printArray(double[] numbers) {

        for (double number : numbers) {
            System.out.println(number);
        }
    }

    public static void checkIfTrue(boolean value) {
        if (value) {
            System.out.println("Prawda");
        }else {
            System.out.println("Nieprawda");
        }
    }



}
