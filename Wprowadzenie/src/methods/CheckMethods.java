package methods;

/**
 * Created by Adrian on 2017-08-01.
 */
public class CheckMethods {

    public static void main(String[] args) {
        PrintMethods.checkIfTrue(isDivisible(5,2));
        PrintMethods.checkIfTrue(isDivisible(4,2));
        PrintMethods.checkIfTrue(isSmallLetter('a'));
        PrintMethods.checkIfTrue(isSmallLetter('A'));

        PrintMethods.checkIfTrue(hasSmallLetter("aaBsSD"));
        PrintMethods.checkIfTrue(hasSmallLetter("ISHUA"));
        PrintMethods.checkIfTrue(hasOnlySmallLetter("abc"));
        PrintMethods.checkIfTrue(hasOnlySmallLetter("abcISHUA"));

    }

    public static boolean isDivisible(int a, int b) {
        return a%b == 0;
    }

    public static boolean isSmallLetter(char c) {
        return c >= 'a' && c <= 'z';
    }

    public static boolean hasSmallLetter(String text) {

        for (char c : text.toCharArray()) {
            if (isSmallLetter(c)) {
                return true;
            }
        }
        return false;

    }

    public static boolean hasOnlySmallLetter(String text) {

        for (char c : text.toCharArray()) {
            if (!isSmallLetter(c)) {
                return false;
            }
        }
        return true;

    }
}
