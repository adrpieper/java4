package methods;

/**
 * Created by Adrian on 2017-07-31.
 */
public class CalcMethods {

    public static void main(String[] args) {
        System.out.println(sum(5,8));
    }

    public static int sum(int a, int b) {
        return a+b;
    }
}
