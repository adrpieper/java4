package autoboxing;

/**
 * Created by Adrian on 2017-08-10.
 */
public class IntegerExample {

    public static void main(String[] args) {
        Integer i1 = 100;
        Integer i2 = 100;
        System.out.println(i1 == i2);
        System.out.println(i1.equals(i2));
        Integer i3 = 1000;
        Integer i4 = 1000;
        System.out.println(i3 == i4);
        System.out.println(i3.equals(i4));

        System.out.println(i3 == 1000);
        System.out.println(i3.equals(1000));

    }
}
