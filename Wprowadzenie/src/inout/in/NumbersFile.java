package inout.in;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-23.
 */
public class NumbersFile {

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File("data/numbers.txt"))){
            while (scanner.hasNextInt()){
                int i = scanner.nextInt();
                integers.add(i);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(integers);
        int sum = 0;
        for (Integer integer : integers) {
            sum+=integer;
        }
        System.out.println("Suma = " + sum);
        System.out.println("Ilość = " + integers.size());
    }
}
