package inout.in;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-23.
 */
public class NamesFile {

    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        try (Scanner fileScanner = new Scanner(new File("data/names.txt"))){
            while (fileScanner.hasNext()){
                names.add(fileScanner.next());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(names);

        Scanner consoleScanner = new Scanner(System.in);
        System.out.println("Podaj literę");
        String letter = consoleScanner.next().toUpperCase();
        for (String name : names) {
            if (name.startsWith(letter)) {
                System.out.println(name);
            }
        }
    }
}
