package inout.in;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-23.
 */
public class MathFile {

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(new File("data/math.txt"))){
            while (scanner.hasNextLine()){
                String line = scanner.nextLine();
                String[] split = line.split(" ");
                int a = Integer.parseInt(split[0]);
                String operation = split[1];
                int b = Integer.parseInt(split[2]);
                System.out.println(a + operation + b + "=" + calculate(a,b,operation));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static int calculate(int a, int b, String operation) {
        switch (operation) {
            case "+":
                return a+b;
            case "-":
                return a-b;
            case "/":
                return a/b;
            case "*":
                return a*b;
        }
        throw new UnsupportedOperationException(operation);
    }
}
