package inout.in;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-23.
 */
public class ReadLineByLineExample {

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(new File("data/math.txt"))){
            while (scanner.hasNextLine()){
                System.out.println(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
