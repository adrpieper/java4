package inout.in.contact;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-23.
 */
public class ContactMain {

    public static void main(String[] args) {
        List<Contact> contacts = loadContacts();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj cyfry");
        String digits = scanner.next();
        for (Contact contact : contacts) {
            if (contact.getNumber().startsWith(digits)) {
                System.out.println(contact.getName());
            }
        }
        System.out.println("Podaj domenę");
        String address = scanner.next();
        for (Contact contact : contacts) {
            if (contact.getEmail().endsWith("@"+address)) {
                System.out.println(contact.getName());
            }
        }
    }

    private static List<Contact> loadContacts() {
        List<Contact> contacts = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File("data/contacts.csv"))){
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                System.out.println(line);
                String[] split = line.split(";");
                System.out.println(split[0]);
                System.out.println(split[1]);
                System.out.println(split[2]);
                Contact contact = new Contact(split[0],split[1],split[2]);
                contacts.add(contact);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return contacts;
    }
}
