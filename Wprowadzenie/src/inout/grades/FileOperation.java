package inout.grades;

import java.util.List;

/**
 * Created by Adrian on 2017-08-25.
 */
public interface FileOperation {
    void saveToFile(List<Integer> grades);
    List<Integer> loadFromFile();
}
