package inout.grades;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 2017-08-25.
 */
public class BinaryFileOperation implements FileOperation {
    @Override
    public void saveToFile(List<Integer> grades) {
        try (DataOutputStream file = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("binary_grades.txt")))) {
            // zapisuję informację o ilości ocen
            file.writeInt(grades.size());
            for (int grade : grades) {
                file.writeByte(grade);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Integer> loadFromFile() {
        try (DataInputStream file = new DataInputStream(new BufferedInputStream(new FileInputStream("binary_grades.txt")))){
            List<Integer> grades = new ArrayList<>();
            int size = file.readInt();
            for (int i = 0; i < size; i++) {
                grades.add((int) file.readByte());
            }
            return grades;
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }
}
