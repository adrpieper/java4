package inout.grades;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-25.
 */
public class TextFileOperation implements FileOperation {

    @Override
    public void saveToFile(List<Integer> grades) {
        try (PrintStream file = new PrintStream("grades.txt")) {
            for (Integer grade : grades) {
                file.println(grade);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Integer> loadFromFile() {
        try (Scanner file = new Scanner(new File("grades.txt"))) {
            List<Integer> grades = new ArrayList<>();
            while (file.hasNextInt()) {
                grades.add(file.nextInt());
            }
            return grades;
        } catch (FileNotFoundException e) {
            return new ArrayList<>();
        }
    }
}
