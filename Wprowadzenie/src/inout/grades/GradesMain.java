package inout.grades;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-24.
 */
public class GradesMain {
    public static void main(String[] args) {
        FileOperation operation = new SerializationFileOperation();
        List<Integer> grades = operation.loadFromFile();
        runProgram(grades);
        operation.saveToFile(grades);

    }

    private static void runProgram(List<Integer> grades) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Dodaj (+), usuń (-) lub wyświetl (show):");
            String operation = scanner.next();
            if (operation.equals("exit")) {
                break;
            }else if (operation.equals("show")) {
                System.out.println(grades);
            }else {
                System.out.println("Podaj ocenę");
                Integer grade = scanner.nextInt();
                if (operation.equals("+")) {
                    grades.add(grade);
                } else if (operation.equals("-")) {
                    grades.remove(grade);
                }
            }
        }
    }
}
