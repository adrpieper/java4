package inout.grades;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 2017-08-25.
 */
public class SerializationFileOperation implements FileOperation {
    @Override
    public void saveToFile(List<Integer> grades) {
        try (ObjectOutputStream file = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("grades_serializacja.txt")))) {
            file.writeObject(grades);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Integer> loadFromFile() {
        try (ObjectInputStream file = new ObjectInputStream(new BufferedInputStream(new FileInputStream("grades_serializacja.txt")))) {
            List<Integer> readNumbers = (List<Integer>) file.readObject();
            return readNumbers;
        } catch (IOException | ClassNotFoundException e) {
            return new ArrayList<>();
        }
    }
}
