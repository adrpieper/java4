package inout.binary;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 2017-08-25.
 */
public class ObjectStreamExample {

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        integers.add(4);
        integers.add(2);
        integers.add(3);
        try(ObjectOutputStream file = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("lista_liczb_serializacja.txt")))) {
            file.writeObject(integers);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream file = new ObjectInputStream(new BufferedInputStream(new FileInputStream("lista_liczb_serializacja.txt")))){
            List<Integer> readNumbers = (List<Integer>) file.readObject();
            System.out.println(readNumbers);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
