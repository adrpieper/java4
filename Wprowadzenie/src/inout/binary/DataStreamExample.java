package inout.binary;

import java.io.*;

/**
 * Created by Adrian on 2017-08-25.
 */
public class DataStreamExample {

    public static void main(String[] args) {

        try(DataOutputStream file = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("dane.txt")))) {
            file.writeInt(4);
            file.writeUTF("Hello World!!!");
            file.writeInt(6);
            file.writeInt(20);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
