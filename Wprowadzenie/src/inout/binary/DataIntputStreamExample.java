package inout.binary;

import java.io.*;

/**
 * Created by Adrian on 2017-08-25.
 */
public class DataIntputStreamExample {

    public static void main(String[] args) {
        try (DataInputStream file = new DataInputStream(new BufferedInputStream(new FileInputStream("dane.txt")))){

            System.out.println(file.readInt());
            System.out.println(file.readInt());
            System.out.println(file.readInt());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
