package oop.club;

/**
 * Created by Adrian on 2017-08-07.
 */
public class ClubMain {

    public static void main(String[] args) {
        Club club = new Club();
        Person person = new Person("Jan", "Kowalski", 16);
        club.enter(person);

    }
}
