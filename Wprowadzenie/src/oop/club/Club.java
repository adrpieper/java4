package oop.club;

/**
 * Created by Adrian on 2017-08-07.
 */
public class Club {

    public void enter(Person person) throws NoAdultException{
        if (person.getAge() < 18) {
            throw new NoAdultException(person);
        }
    }
}
