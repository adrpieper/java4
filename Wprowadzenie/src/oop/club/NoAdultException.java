package oop.club;

/**
 * Created by Adrian on 2017-08-07.
 */
public class NoAdultException extends RuntimeException {
    private final Person person;

    public NoAdultException(Person person) {
        super(person.getName() + " nie jest dorosły");
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
}
