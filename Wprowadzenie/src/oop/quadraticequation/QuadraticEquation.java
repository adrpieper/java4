package oop.quadraticequation;


import java.util.Map;

/**
 * Created by Adrian on 2017-08-04.
 */

// Reprezentuje równanie a*x^2+b*x+c
public class QuadraticEquation {
    private double a;
    private double b;
    private double c;

    public static void main(String[] args) {
        QuadraticEquation equation = new QuadraticEquation(1,1,2);
        System.out.println("delta = " + equation.calcDelta());
        if (equation.calcDelta() >= 0 ) {
            System.out.println("X1 = " + equation.calcX1());
            System.out.println("X2 = " + equation.calcX2());
        }else {
            System.out.println("Delta jest ujemna ");
        }
    }

    private double calcX1() {
        if (calcDelta()>=0) {
            return (-b - Math.sqrt(calcDelta())) / (2 * a);
        }else {
            throw new NegativeDeltaException();
        }
    }

    private double calcX2() {
        if (calcDelta()>=0) {
            return (-b + Math.sqrt(calcDelta())) / (2 * a);
        }else {
            throw new NegativeDeltaException();
        }
    }

    private double calcDelta() {
        return b*b - 4*a*c;
    }

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }


}
