package oop.figures;

/**
 * Created by Adrian on 2017-08-03.
 */
public class Circle implements Figure{
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    public double countArea() {
        return r * r * Math.PI;
    }

    @Override
    public double countCircumference() {
        return 2 * r * Math.PI;
    }
}
