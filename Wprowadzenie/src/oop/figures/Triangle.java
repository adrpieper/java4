package oop.figures;

import oop.point.Point;

/**
 * Created by Adrian on 2017-08-04.
 */
public class Triangle implements Figure {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle(Point A, Point B, Point C) {
        this.a = A.distance(B);
        this.b = B.distance(C);
        this.c = C.distance(A);
    }

    @Override
    public double countCircumference() {
        return a+b+c;
    }

    @Override
    public double countArea (){
        double p = (a+b+c)*0.5;
        return Math.sqrt((p*(p-a)*(p-b)*(p-c)));
    }

}
