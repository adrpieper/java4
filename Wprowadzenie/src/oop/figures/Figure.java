package oop.figures;

/**
 * Created by Adrian on 2017-08-07.
 */
public interface Figure {

    double countArea();
    double countCircumference();

}
