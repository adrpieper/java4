package oop.figures;

import oop.point.Point;

/**
 * Created by Adrian on 2017-08-03.
 */
public class FiguresTest {

    public static void main(String[] args) {
        Square square = new Square(10);
        Circle circle = new Circle(2);
        Triangle triangle = new Triangle(2,3,2);

        Figure[] figures = {square, circle, triangle};
        for (Figure figure : figures) {
            print(figure);
        }

        System.out.println("Kwadrat");
        print(square);
        System.out.println("Trójkąt");
        print(triangle);
        System.out.println("Koło");
        print(circle);
    }

    public static void print(Figure figure) {
        System.out.println("area : " + figure.countArea());
        System.out.println("circumference : " + figure.countCircumference());
    }
}
