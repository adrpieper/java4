package oop.figures;

/**
 * Created by Adrian on 2017-08-03.
 */
public class Square implements Figure{
    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double countArea() {
        return a*a;
    }

    @Override
    public double countCircumference() {
        return 4*a;
    }
}
