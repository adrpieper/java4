package oop.person.bad;

/**
 * Created by Adrian on 2017-08-02.
 */
public class OsobaMain {

    public static void main(String[] args) {
        Osoba jan = new Osoba();
        jan.imie = "Jan";
        jan.wiek = 20;
        Osoba adam = new Osoba();
        adam.imie = "Adam";
        adam.wiek = 24;
        Osoba ania = new Osoba();
        ania.imie = "Ania";
        ania.wiek = 18;

        Osoba[] osoby = {jan, adam, ania};

        for (Osoba osoba : osoby) {
            System.out.println(osoba.imie);
            System.out.println(osoba.wiek);
        }
    }
}
