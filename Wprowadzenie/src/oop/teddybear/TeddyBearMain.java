package oop.teddybear;

/**
 * Created by Adrian on 2017-08-02.
 */
public class TeddyBearMain {

    public static void main(String[] args) {
        TeddyBear teddyBear = new TeddyBear("Puchatek");
        teddyBear.introduce();
    }
}
