package oop.teddybear;

/**
 * Created by Adrian on 2017-08-02.
 */
public class TeddyBear {
    private String name;

    public TeddyBear(String name) {
        this.name = name;
    }

    public void introduce() {
        System.out.println(name);
    }
}
