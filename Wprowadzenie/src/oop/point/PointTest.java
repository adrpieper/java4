package oop.point;

import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-03.
 */
public class PointTest {

    public static void main(String[] args) {
        Point p1 = new Point(4,6);
        System.out.println("x=" + p1.getX());
        System.out.println("y=" + p1.getY());
        p1.setX(10);
        p1.setY(11);
        System.out.println("x=" + p1.getX());
        System.out.println("y=" + p1.getY());
        p1.move(-2,3);
        p1.print();

        Scanner scanner = new Scanner(System.in);
        Point p2 = newPoint(scanner);
        Point p3 = newPoint(scanner);
        p2.print();
        p3.print();
        System.out.println(p2.distance(p3));
    }

    private static Point newPoint(Scanner scanner) {
        System.out.println("Podaj x ");
        int x = scanner.nextInt();
        System.out.println("Podaj y ");
        int y = scanner.nextInt();
        Point p = new Point(x,y);
        return p;
    }
}
