package oop.point;

/**
 * Created by Adrian on 2017-08-03.
 */
public class Point {

    private int x;
    private int y;

    public Point() {
        // Wartości domyśle dla pól,
        // są im nadawane w momencie tworzenia obiektu
        // x = 0; Dla typu int wartością domyślą jest 0
        // y = 0;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void print() {
        System.out.println("{"+x+","+y+"}");
    }

    public void move(int dx, int dy) {
        x += dx;
        y += dy;
    }

    public double distance(Point point) {
        int xDistance = x - point.x;
        int yDistance = y - point.y;
        return Math.sqrt(xDistance * xDistance + yDistance * yDistance);
    }
}
