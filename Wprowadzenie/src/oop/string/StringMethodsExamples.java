package oop.string;

/**
 * Created by Adrian on 2017-08-04.
 */
public class StringMethodsExamples {

    public static void main(String[] args) {
        String[] words = "A,B,AA,AB,BA,BB,AAA,AAB,ABA,ABB,BAA,BAB,BBA,BBB".split(",");

        System.out.println("Wszystkie słowa");
        for (String word : words) {
            System.out.print(word + ' ');
        }
        System.out.println();
        System.out.println("Wszystkie słowa zaczynające się od AB");
        for (String word : words) {
            if (word.startsWith("AB")) {
                System.out.print(word + ' ');
            }
        }
        System.out.println();

        System.out.println("Wszystkie słowa dłuższe niż 2 ");
        for (String word : words) {
            if (word.length() > 2) {
                System.out.print(word + ' ');
            }
        }
        System.out.println();
        System.out.println("Wszystkie słowa zawierające AB");
        for (String word : words) {
            if (word.contains("AB")) {
                System.out.print(word + ' ');
            }
        }
        System.out.println();
        System.out.println("Wszystkie słowa kończące się od B");
        for (String word : words) {
            if (word.endsWith("B")) {
                System.out.print(word + ' ');
            }
        }
        System.out.println();

        System.out.println("Słowa polączone spacją");
        String join = String.join(" ", words);
        System.out.println(join);

    }
}
