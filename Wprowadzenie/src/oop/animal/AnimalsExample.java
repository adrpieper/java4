package oop.animal;

/**
 * Created by Adrian on 2017-08-07.
 */
public class AnimalsExample {

    public static void main(String[] args) {

        Animal[] animals = {new Cat(), new Dog()};

        Animal animal = animals[0];
        Cat cat = (Cat) animals[0];
        //...
        animal = animals[1];

        animal.makeVoice();
        cat.makeVoice();

        for (Animal a : animals) {
            a.makeVoice();
        }
    }
}
