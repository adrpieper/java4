package oop.animal;

/**
 * Created by Adrian on 2017-08-07.
 */
public class Dog implements Animal {

    @Override
    public void makeVoice() {
        System.out.println("Wow, wow!");
    }
}
