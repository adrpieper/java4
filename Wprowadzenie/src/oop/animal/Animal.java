package oop.animal;

/**
 * Created by Adrian on 2017-08-07.
 */
public interface Animal {
    void makeVoice();
}
