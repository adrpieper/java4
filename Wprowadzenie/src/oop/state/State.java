package oop.state;

import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-02.
 */
public class State {
    private String name;
    private int population;
    private int area;
    private int pkb;

    public State(String name, int population, int area, int pkb) {
        this.name = name;
        this.population = population;
        this.area = area;
        this.pkb = pkb;
    }

    public String getName() {
        return name;
    }

    public int getPopulation() {
        return population;
    }

    public int getArea() {
        return area;
    }

    public int getPKB() {
        return pkb;
    }

    public static void printAll(State[] states) {
        System.out.println("Wszystkie państwa");
        for (State state : states) {
            formatState(state);
        }
    }

    private static void formatState(State state) {
        System.out.printf("name: %13s  population: %,16d area: %,12d pkb: %,12d \n", state.getName(), state.getPopulation(), state.getArea(), state.getPKB());
    }

    public static void printAll(State[] states, char firstLetter) {
        System.out.println("Wszystkie na literę " + firstLetter);
        for (State state : states) {
            if (state.getName().charAt(0) == firstLetter) {
                formatState(state);
            }
        }
    }

    public static void printAll(State[] states, String prefix) {
        System.out.println("Wszystkie państwa zaczynające się od " +prefix);
        for (State state : states) {
            if (state.getName().startsWith(prefix)) {
                formatState(state);
            }
        }
    }

    public static State findBiggest(State[] states) {
        State result = states[0];

        for (int i = 1; i < states.length; i++) {
            if (result.getArea() < states[i].getArea()) {
                result = states[i];
            }
        }

        return result;
    }

    public static State findStateWithBiggestPopulation(State[] states) {
        State result = states[0];

        for (int i = 1; i < states.length; i++) {
            if (result.getPopulation() < states[i].getPopulation()) {
                result = states[i];
            }
        }

        return result;
    }

    public static State findStateWithBiggestPKB(State[] states) {
        State result = states[0];

        for (int i = 1; i < states.length; i++) {
            if (result.getPKB() < states[i].getPKB()) {
                result = states[i];
            }
        }

        return result;
    }

    public static State createNewFromUser(Scanner scanner) {
        System.out.println("Podaj nazwę państwa");
        String name = scanner.next();
        System.out.println("Podaj ilość mieszkańców");
        int population = scanner.nextInt();
        System.out.println("Podaj powierzchnię");
        int area = scanner.nextInt();
        System.out.println("Podaj PKB");
        int pkb = scanner.nextInt();

        return new State(name, population, area, pkb);
    }
}
