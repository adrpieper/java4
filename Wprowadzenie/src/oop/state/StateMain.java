package oop.state;

import java.util.Scanner;

/**
 * Created by Adrian on 2017-08-02.
 */
public class StateMain {

    public static void main(String[] args) {
        State[] states = {
                new State("Polska", 38523261, 312685, 467591),
                new State("USA", 323995528, 9526468, 18569100),
                new State("Rosja", 142355415, 17098242, 1280731),
                new State("Chiny", 1373541278, 9596960, 11218281),
        };

        State.printAll(states);
        State.printAll(states,'P');
        State.printAll(states,"Ro");
        State biggestState = State.findBiggest(states);

        System.out.println("Największe państwo to : " + biggestState.getName());
        State biggestPopulation = State.findStateWithBiggestPopulation(states);
        System.out.println("Najludniejsze państwo to : " + biggestPopulation.getName());
        State richest = State.findStateWithBiggestPKB(states);
        System.out.println("Najbogatsze państwo to : " + richest.getName());

        Scanner scanner = new Scanner(System.in);
        State state = State.createNewFromUser(scanner);
        System.out.println("Nowe państwo " + state.getName());

    }
 }
