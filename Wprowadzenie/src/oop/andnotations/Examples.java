package oop.andnotations;

/**
 * Created by Adrian on 2017-08-07.
 */
public class Examples {

    public static void main(String[] args) {
        print("Hello world!");

        if (2 != 2) {
            System.out.println("cowolwiek");
        }
    }

    @Deprecated
    public static void print(String text) {
        System.out.println(text);
    }


    @SuppressWarnings("unused")
    public static void cos() {

    }
}
