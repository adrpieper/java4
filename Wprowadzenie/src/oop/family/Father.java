package oop.family;

/**
 * Created by Adrian on 2017-08-04.
 */
public class Father extends FamilyMember {

    public Father(String name) {
        super(name);
    }

    @Override
    public void introduce() {
        System.out.println("I’m a father. My name is " + getName());
    }
}
