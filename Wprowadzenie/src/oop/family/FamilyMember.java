package oop.family;

/**
 * Created by Adrian on 2017-08-04.
 */
public abstract class FamilyMember {
    private String name;

    public FamilyMember(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void introduce();
}
