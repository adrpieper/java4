package oop.family;

/**
 * Created by Adrian on 2017-08-04.
 */
public class FamilyProgram {

    public static void main(String[] args) {
        Mother mother = new Mother("Anna");
        Father father = new Father("Jan");
        Son son = new Son("Janusz");
        Daughter daughter = new Daughter("Grażyna");

        FamilyMember [] members = {mother, father, son, daughter};

        for (FamilyMember member : members) {
            member.introduce();
        }
    }
}
