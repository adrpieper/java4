package oop.family;

/**
 * Created by Adrian on 2017-08-04.
 */
public class Son extends FamilyMember {
    public Son(String name) {
        super(name);
    }

    @Override
    public void introduce() {
        System.out.println("I’m a son. My name is " + getName());
    }
}
