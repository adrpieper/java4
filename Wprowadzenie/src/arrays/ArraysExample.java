package arrays;

/**
 * Created by Adrian on 2017-07-28.
 */
public class ArraysExample {

    public static void main(String[] args) {
        int[] numbers = {1,2,3,10};
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers.length);
        numbers[2] = 10;
        System.out.println(numbers[2]);
        System.out.println(numbers[0] + numbers[1]);

        arrayOfWords();

        int [][] arrayOfarrays = new int[4][];
        System.out.println(arrayOfarrays[0]);
        arrayOfarrays[0] = numbers;
        System.out.println(arrayOfarrays[0][1]);
        numbers[1] = 10;
        System.out.println(arrayOfarrays[0][1]);
    }

    private static void arrayOfWords() {
        String[] words = new String[3];
        System.out.println(words.length);
        System.out.println(words[1]);
        words[1] = "Jan";
        words[1] = "Roman";
        System.out.println(words[1]);
    }
}
