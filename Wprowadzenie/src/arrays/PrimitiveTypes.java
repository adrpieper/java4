package arrays;

/**
 * Created by Adrian on 2017-07-28.
 */
public class PrimitiveTypes {
    public static void main(String[] args) {
        int a = 3;
        int b = 2;
        a = b;
        b = 10;
        System.out.println(a);
        System.out.println(b);
    }
}
