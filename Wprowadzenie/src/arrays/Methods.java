package arrays;

/**
 * Created by Adrian on 2017-07-28.
 */
public class Methods {

    public static void main(String[] args) {
        int [] numbers = {1, 2, 3, 5, 1};
        int sumOfNumbers = sum(numbers);
        System.out.println("suma liczb : " + sumOfNumbers);
        System.out.println("iloczyn liczb : " + product(numbers));
    }

    // Iloczyn
    public static int product(int[] numbers) {
        int result = 1;
        for (int number : numbers) {
            result *= number;
        }
        return result;
    }

    public static int sum(int[] numbers) {
        int result = 0;
        for (int number : numbers) {
            result += number;
        }
        return result;
    }
}
