package arrays;

import java.util.Scanner;

/**
 * Created by Adrian on 2017-07-28.
 */
public class ArrayFromUser {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj długość: ");
        int length = scanner.nextInt();
        String[] words = new String[length];
        for (int i = 0; i < words.length; i++) {
            System.out.println("Podaj " + i + ". wartość: ");
            words[i] = scanner.next();
        }

        System.out.println("Zawartość tablicy");
        for (String word : words) {
            System.out.println(word);
        }

    }
}
