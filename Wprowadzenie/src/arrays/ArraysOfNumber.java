package arrays;

/**
 * Created by Adrian on 2017-07-28.
 */
public class ArraysOfNumber {

    public static void main(String[] args) {
        int[] numbers =  {1,3,5,10};

        System.out.println("all elements");
        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers[3]);

        System.out.println("for");
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println("for reverse");
        for (int i = numbers.length-1; i >= 0; i--) {
            System.out.println(numbers[i]);
        }

        System.out.println("for each");
        for (int i : numbers) {
            System.out.println(i);
        }


    }
}
