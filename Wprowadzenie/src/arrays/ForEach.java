package arrays;

/**
 * Created by Adrian on 2017-07-28.
 */
public class ForEach {

    public static void main(String[] args) {
        String[] words = {"Jan", "Roman"};
        for (String word : words) {
            System.out.println(word);
        }
    }
}


