package arrays;

/**
 * Created by Adrian on 2017-07-28.
 */
public class ArraysOfNames {

    public static void main(String[] args) {
        String[] names = {"Jan", "Adam", "Ewa", "Zofia", "Ania"};

        System.out.println("for-each");
        for (String name : names) {
            System.out.println(name);
        }
        System.out.println("for");
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

        System.out.println("Co drugie imię");
        for (int i = 0; i < names.length; i+=2) {
            System.out.println(names[i]);
        }

        System.out.println("Imiona na A");
        for (String name : names) {
            if (name.startsWith("A")) {
                System.out.println(name);
            }
        }

    }
}
