package shapes;

/**
 * Created by Adrian on 2017-08-01.
 */
public class Geometry {

    public static void main(String[] args) {
        System.out.println("circleArea(2) = " + circleArea(2));
        System.out.println("cylinderVolume(2,3) = " + cylinderVolume(2,3));
    }

    public static double circleArea(double r) {
        return r*r*Math.PI;
    }

    public static double cylinderVolume(double r, double h) {
        return circleArea(r) * h;
    }

    private static double pyramidVolume(double a, double h) {
        return h*squareArea(a)/3;
    }

    private static double cubeVolume(double a) {
        return a*cubeArea(a);
    }

    private static double coneVolume(double r, double h) {
        return cylinderVolume(r,h)/3;
    }

    private static double cubeArea(double a) {
        return 6*squareArea(a);
    }

    private static double squareArea(double a) {
        return a*a;
    }
}
