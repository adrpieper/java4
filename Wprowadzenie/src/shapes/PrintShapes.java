package shapes;

/**
 * Created by Adrian on 2017-08-01.
 */
public class PrintShapes {

    public static void main(String[] args) {
        printRect(8, 2);
        printRect(2, 8);
        printTriangle(8);
    }

    public static void printRect(int a, int b) {
        for (int i = 0; i < a; i++) {
            printLine('*',b);
        }
    }

    public static void printTriangle(int a) {
        for (int i = 0; i < a; i++) {
            printLine('*',i+1);
        }
    }
    public static void printLine(char c, int length) {
        for (int i = 0; i < length; i++) {
            System.out.print(c);
        }
        System.out.println();
    }
}
