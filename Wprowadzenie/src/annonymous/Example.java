package annonymous;


/**
 * Created by Adrian on 2017-08-17.
 */
public class Example {

    public static void main(String[] args) {
        Animal cat = new Animal() {
            @Override
            public void makeNoise() {
                System.out.println("Miał!");
            }
        };

        cat.makeNoise();
    }
}
