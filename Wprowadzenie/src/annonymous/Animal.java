package annonymous;

/**
 * Created by Adrian on 2017-08-17.
 */
public interface Animal {
    void makeNoise();
}
