package repetition.overloading;

/**
 * Created by Adrian on 2017-08-28.
 */
public class Example {

    public static void main(String[] args) {
        print("Hello World!");
        int a = 'a';
        print(a);
        String s = "Hello World!";
        Object o = s;
        print(o);
    }

    public static void print(Object text) {
        System.out.println("Object : " +text);
    }

    public static void print(String text) {
        System.out.println("Text : " +text);
    }

    public static void print(char sign) {
        System.out.println("Znak : " + sign);
    }

    public static void print(int number) {
        System.out.println("Liczba : " +number);
    }
}
