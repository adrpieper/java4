package repetition.overriding;

/**
 * Created by Adrian on 2017-08-28.
 */
public class Cat extends Animal {

    @Override
    public void makeNoise() {
        System.out.println("Miał!");
    }

    public void jump() {
        System.out.println("Skaczę");
    }
}
