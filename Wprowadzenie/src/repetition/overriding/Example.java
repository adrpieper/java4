package repetition.overriding;

/**
 * Created by Adrian on 2017-08-28.
 */
public class Example {

    public static void main(String[] args) {
        Animal animalCat = new Cat();
        Animal animalAnimal = new Animal();
        Cat catCat = new Cat();
        //Cat catAnimal = new Animal(); Błąd
        animalCat.makeNoise();
        animalAnimal.makeNoise();
        catCat.makeNoise();
        catCat.jump();
        //animalCat.jump(); Błąd
        ((Cat) animalCat).jump();
        // ((Cat) animalAnimal).jump(); Błąd
    }
}
