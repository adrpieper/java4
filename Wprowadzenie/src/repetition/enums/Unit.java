package repetition.enums;

/**
 * Created by Adrian on 2017-08-28.
 */
public enum Unit {
    CM(10),
    MM(1),
    M(1000);

    private int mm;

    Unit(int mm) {
        this.mm = mm;
    }

    public int getMM() {
        return mm;
    }
}
