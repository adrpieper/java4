package repetition.enums;

/**
 * Created by Adrian on 2017-08-28.
 */
public class Segment {
    private int lenght;
    private Unit unit;

    public Segment(int lenght, Unit unit) {
        this.lenght = lenght;
        this.unit = unit;
    }

    public int getLenght() {
        return lenght;
    }

    public Unit getUnit() {
        return unit;
    }

    public int toMM() {
        return lenght*unit.getMM();
    }
}
