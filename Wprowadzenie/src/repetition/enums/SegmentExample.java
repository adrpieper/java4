package repetition.enums;

/**
 * Created by Adrian on 2017-08-28.
 */
public class SegmentExample {

    public static void main(String[] args) {
        Segment segment = new Segment(10,Unit.CM);
        System.out.println(segment.toMM());

        System.out.println(segment.getUnit().name());
        System.out.println("Dostępne jednostki");
        for (Unit unit : Unit.values()) {
            System.out.println(unit.toString() + " " + unit.ordinal());
            Unit cm = Unit.valueOf("CM");
        }

    }
}
