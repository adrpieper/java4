package structures.list;

/**
 * Created by Adrian on 2017-08-30.
 */
public class SimpleLinkedList implements SimpleList {
    private int amount;
    private Element first;
    private Element last;

    @Override
    public void add(int value) {
        Element newElement = new Element(value);
        if (amount == 0) {
            first = newElement;
        } else {
            join(last, newElement);
        }
        last = newElement;
        amount++;
    }

    private void join(Element left, Element right) {
        left.next = right;
        right.prev = left;
    }

    @Override
    public void add(int value, int index) {
        if (index == amount) {
            add(value);
        } else {
            Element element = find(index);
            addBefore(element,value);
        }
    }

    private void addBefore(Element element,int value) {
        Element newElement = new Element(value);
        if (first == element) {
            first = newElement;
        }else {
            Element prev = element.prev;
            join(prev,newElement);
        }
        join(newElement,element);
        amount++;
    }

    @Override
    public int get(int index) {
        Element element = find(index);
        return element.value;
    }

    private Element find(int index) {
        Element element = first;
        for (int i = 0; i < index; i++) {
            element = element.next;
        }
        return element;
    }

    @Override
    public boolean contain(int value) {
        return findByValue(value) != null;
    }

    @Override
    public void remove(int index) {
        Element element = find(index);
        remove(element);
    }

    private void remove(Element element) {
        Element prev = element.prev;
        if (prev != null) {
            prev.next = element.next;
        }else {
            first = element.next;
        }

        Element next = element.next;
        if (next != null) {
            next.prev = element.prev;
        }else {
            last = element.prev;
        }
        amount--;
    }

    @Override
    public void removeValue(int value) {
        Element element = findByValue(value);
        remove(element);
    }

    private Element findByValue(int value) {
        Element element = first;
        while (element != null){
            if (element.value == value) {
                return element;
            }
            element = element.next;
        }
        return null;
    }

    @Override
    public int size() {
        return amount;
    }

    /*
    Klasa wewnętrzna reprezentująca pojedynczy elementy listy.
    Klasa jest prywatna, ponieważ służy jedynie implementacji
    struktury listy i nie powinna być widoczna poza nią.
     */
    private static class Element {
        int value;
        Element prev;
        Element next;

        public Element(int value) {
            this.value = value;
        }
    }
}
