package structures.list;

/**
 * Created by Adrian on 2017-08-30.
 */
public class SimpleListTest {

    public static void main(String[] args) {
        SimpleList list = new SimpleLinkedList();
        list.add(0);
        list.add(2);
        list.add(5);
        System.out.println("Rozmiar : " + list.size());
        System.out.println("Element o indeksie 0 : " + list.get(0));
        System.out.println("Element o indeksie 1 : " + list.get(1));
        System.out.println("Element o indeksie 2 : " + list.get(2));
        list.remove(1);
        System.out.println("Rozmiar : " + list.size());
        System.out.println("Element o indeksie 0 : " + list.get(0));
        System.out.println("Element o indeksie 1 : " + list.get(1));
        list.add(100,2);
        list.add(10,0);
        list.add(20,1);
        System.out.println("Rozmiar : " + list.size());
        System.out.println("Element o indeksie 0 : " + list.get(0));
        System.out.println("Element o indeksie 1 : " + list.get(1));
        System.out.println("Element o indeksie 2 : " + list.get(2));
        System.out.println("Element o indeksie 3 : " + list.get(3));
        System.out.println("Element o indeksie 4 : " + list.get(4));
        System.out.println("Czy zawiera 5  : " + list.contain(5));
        System.out.println("Czy zawiera 50 : " + list.contain(50));
    }
}
