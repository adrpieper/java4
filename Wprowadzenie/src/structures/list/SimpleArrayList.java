package structures.list;

/**
 * Created by Adrian on 2017-08-30.
 */
public class SimpleArrayList implements SimpleList {
    private int amount = 0;
    private int[] data = new int[8];

    public void add(int value) {

        checkMemory();
        data[amount] = value;
        amount += 1;
    }

    private void checkMemory() {
        if (amount == data.length) {
            int[] buffor = new int[amount * 2];
            for (int i = 0; i < amount; i++) {
                buffor[i] = data[i];
            }
            data = buffor;
        }
    }

    @Override
    public void add(int value, int index) {
        checkMemory();
        for (int i = amount - 1; i >= index; i--) {
            data[i + 1] = data[i];
        }
        data[index] = value;
        amount += 1;
    }

    @Override
    public int get(int index) {
        if (index >= amount || index < 0) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        return data[index];
    }

    @Override
    public boolean contain(int value) {
        for (int i = 0; i < amount; i++) {
            if (data[i] == value) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void remove(int index) {
        for (int i = index; i < amount - 1; i++) {
            data[i] = data[i + 1];
        }
        amount -= 1;

        if (amount < data.length / 4 && data.length > 8) {
            int[] buffor = new int[data.length / 2];
            for (int i = 0; i < amount; i++) {
                buffor[i] = data[i];
            }
            data = buffor;
        }
    }

    @Override
    public void removeValue(int value) {
        for (int i = 0; i < amount; i++) {
            if (data[i] == value) {
                remove(i);
                break;
            }
        }
    }

    @Override
    public int size() {
        return amount;
    }
}
