package structures.list;

/**
 * Created by Adrian on 2017-08-30.
 */
public interface SimpleList {
    void add(int value);
    void add(int value, int index);
    int get(int index);
    boolean contain(int value);
    void remove(int index);
    void removeValue(int value);
    int size();
}
