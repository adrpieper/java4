package structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Adrian on 2017-08-31.
 */
public class IteratorExample {

    public static void main(String[] args) {

        List<Integer> integers = new ArrayList<>();
        integers.add(5);
        integers.add(8);
        integers.add(9);

        Iterator<Integer> iterator = integers.iterator();


        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
