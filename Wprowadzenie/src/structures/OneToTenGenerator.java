package structures;

import java.util.Iterator;

/**
 * Created by Adrian on 2017-08-31.
 */
public class OneToTenGenerator implements Iterable<Integer>{

    @Override
    public Iterator<Integer> iterator() {
        return new NumbersIterator();
    }

    private static class NumbersIterator implements Iterator<Integer> {

        private int number = 1;

        @Override
        public boolean hasNext() {
            return number <= 10;
        }

        @Override
        public Integer next() {
            return number++;
        }
    }
}
