package structures;

import java.util.Iterator;

/**
 * Created by Adrian on 2017-08-31.
 */
public class IteratorImplementationExample {

    public static void main(String[] args) {
        OneToTenGenerator generator = new OneToTenGenerator();
        for (int number : generator) {
            System.out.println("liczba : " + number);
        }

        Iterator<Integer> iterator = generator.iterator();
        while (iterator.hasNext()) {
            int number = iterator.next();
            System.out.println("liczba : " + number);
        }
    }
}
