package structures.queue;

/**
 * Created by Adrian on 2017-08-31.
 */
public class QueueTest {
    public static void main(String[] args) {
        SimpleQueue simpleQueue = new SimpleArrayQueue();
        System.out.println("Czy pusta : " + simpleQueue.isEmpty());
        simpleQueue.offer(2);
        simpleQueue.offer(4);
        simpleQueue.offer(5);
        System.out.println("Czy pusta : " + simpleQueue.isEmpty());
        while (!simpleQueue.isEmpty()){
            System.out.println(simpleQueue.peek());
            System.out.println(simpleQueue.poll());
        }
    }
}
