package structures.queue;

/**
 * Created by Adrian on 2017-08-31.
 */
public interface SimpleQueue {
    boolean isEmpty(); // sprawdza, czy kolejka jest pusta
    void offer(int value); // dodaje element do kolejki
    int poll(); // zwraca i usuwa element z kolejki
    int peek(); // zwraca element z kolejki
}
