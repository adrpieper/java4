package structures.queue;

import java.util.NoSuchElementException;

/**
 * Created by Adrian on 2017-08-31.
 */
public class SimpleArrayQueue implements SimpleQueue {
    private int first = 0;
    private int last = 0;
    private int amount = 0;
    private int[] data = new int[8];

    @Override
    public boolean isEmpty() {
        return amount == 0;
    }

    @Override
    public void offer(int value) {
        // TODO sprawdzenie, czy jest miejsce dla nowego elementy
        data[last] = value;
        last = (last + 1) % data.length;
        amount++;
    }

    @Override
    public int poll() {
        if (amount == 0) {
            throw new NoSuchElementException("Queue is empty");
        }
        amount--;
        return data[first++];
    }

    @Override
    public int peek() {
        if (amount == 0) {
            throw new NoSuchElementException("Queue is empty");
        }
        return data[first];
    }
}
