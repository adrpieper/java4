package time;

import java.time.*;
import java.time.temporal.ChronoUnit;

/**
 * Created by Adrian on 2017-08-08.
 */
public class Examples {

    public static void main(String[] args) {


        LocalTime.now();
        LocalTime.of(15,20);
        LocalTime.parse("15:35");

        LocalDateTime.now();
        LocalDateTime.of(2017,5,16,5,30);
        LocalDateTime.parse("2017-05-15T15:35");

        System.out.println(LocalDateTime.now());
        System.out.println(LocalDateTime.of(2017,5,16,5,30));
        System.out.println(LocalDateTime.parse("2017-05-15T15:35"));


        // 8 miesięcy temu
        Period eightMonths = Period.ofMonths(8);
        LocalDateTime eightMonthsAgo = LocalDateTime.now().minus(eightMonths);

        // 5 godzin temu
        Duration fiveHour = Duration.ofHours(5);
        LocalDateTime fiveHoursAgo = LocalDateTime.now().minus(fiveHour);

        System.out.println(LocalDateTime.now().toLocalTime());

        LocalDate startData = LocalDate.of(2011, 5, 12);
        LocalDate endData = LocalDate.of(2015, 6, 16);
        Period.between(startData, endData);
        Period of = Period.of(4, 5, 60);
        System.out.println(of.getDays());
        Period.ofWeeks(6);
        Period.parse("P2Y1M4D");

        LocalTime firstTime = LocalTime.of(5,20);
        LocalTime secondTime = LocalTime.of(8,50);
        firstTime.isAfter(secondTime);
        System.out.println();
        System.out.println(firstTime.isBefore(secondTime));
        System.out.println(firstTime.until(secondTime, ChronoUnit.HOURS));
        System.out.println(firstTime.until(secondTime, ChronoUnit.MINUTES));
        System.out.println(firstTime.until(secondTime, ChronoUnit.MILLIS));


        LocalDateTime now = LocalDateTime.now();
        LocalDate nowDate = now.toLocalDate();
        LocalTime nowTime = now.toLocalTime();
    }
}
