package time;

import java.time.*;

/**
 * Created by Adrian on 2017-08-08.
 */
public class TimePrinter {

    public static void main(String[] args) {
        System.out.println("Dzisiaj :");
        print(LocalDate.now());
        System.out.println("2017-05-04 :");
        print(LocalDate.of(2017, 5, 4));
        System.out.println("Wczoraj :");
        print(LocalDate.now().minusDays(1));
        System.out.println("Jutro :");
        print(LocalDate.now().plusDays(1));
        System.out.println("Rok temu :");
        print(LocalDate.now().minusYears(1));

        System.out.println("Aktualny czas :");
        print(LocalTime.now());
        System.out.println("15:45 :");
        print(LocalTime.of(15, 45));
        System.out.println("5 minut temu :");
        print(LocalTime.now().minusMinutes(5));
        System.out.println("3 godziny temu :");
        print(LocalTime.now().minusHours(3));
        System.out.println("Za 50 minut :");
        print(LocalTime.now().plusMinutes(50));


        System.out.println("Dzisiejsza data i aktualny czas :");
        print(LocalDateTime.now());
        System.out.println("Koniec zajęć :");
        print(LocalDateTime.of(LocalDate.now(), LocalTime.of(17, 00)));
        //print(LocalDate.now().atTime(17,00));
        System.out.println("15-05-2010 godzina 16:50 :");
        print(LocalDateTime.of(2010, 5, 15, 16, 50));
        System.out.println("Za 40 minut :");
        print(LocalDateTime.now().plusMinutes(40));

        print(LocalDate.now().until(LocalDate.of(2018,1,11)));
        print(LocalDate.of(2017,7,24).until(LocalDate.now()));

        print(Duration.ofHours(3).plusMinutes(30).plusSeconds(20));
    }

    public static void print(LocalDate date) {
        System.out.println("rok : " + date.getYear());
        System.out.println("miesiąc : " + date.getMonth());
        System.out.println("dzień : " + date.getDayOfMonth());
    }

    public static void print(LocalTime time) {
        System.out.println("godzina : " + time.getHour());
        System.out.println("minuta : " + time.getMinute());
        System.out.println("sekunda : " + time.getSecond());
    }

    public static void print(LocalDateTime localDateTime) {
        System.out.println("data : " + localDateTime.toLocalDate());
        System.out.println("czas : " + localDateTime.toLocalTime());
    }

    public static void print(Period period) {
        System.out.println(period.getYears() + "lat, "
                + period.getMonths() + "miesięcy, "
                + period.getDays() + "dni");
    }

    public static void print(Duration duration) {
        long hours = duration.toHours();
        Duration durationWithoutHour = duration.minusHours(hours);
        Duration durationWithoutMinuts = durationWithoutHour.minusMinutes(durationWithoutHour.toMinutes());
        String text = hours + " godzin " + durationWithoutHour.toMinutes() + " minut " + durationWithoutMinuts.getSeconds() + " seconds";
        System.out.println(text);
    }
}
