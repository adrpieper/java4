package time.schedule;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Created by Adrian on 2017-08-08.
 */
public class Record {
    private String name;
    private LocalDateTime start;
    private LocalDateTime end;

    public Record(String name, LocalDate date, LocalTime startTime, LocalTime endTime) {
        this.name = name;
        this.start = date.atTime(startTime);
        this.end = date.atTime(endTime);
    }

    public Record(String name, LocalDateTime start, LocalDateTime end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public LocalDateTime getStart() {
        return start;
    }
}
