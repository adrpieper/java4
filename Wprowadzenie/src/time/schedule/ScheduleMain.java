package time.schedule;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Created by Adrian on 2017-08-08.
 */
public class ScheduleMain {

    public static void main(String[] args) {
        Record[] records = {
                new Record(
                        "Spotkanie biznesowe",
                        LocalDate.of(2017, 10, 12),
                        LocalTime.of(15, 30),
                        LocalTime.of(18, 30)),
                new Record(
                        "Rodzinny obiad",
                        LocalDate.of(2017, 10, 13),
                        LocalTime.of(13, 0),
                        LocalTime.of(16, 0)),
                new Record(
                        "Wakacje",
                        LocalDateTime.of(2017, 7, 12, 0, 0),
                        LocalDateTime.of(2017, 7, 24, 0, 0)
                )
        };

        System.out.println("Wszystkie wydarzenia: ");
        print(records);
        System.out.println("Wydarzenia odbywające się przed 13.10.2017: ");
        printBefore(records, LocalDate.of(2017, 10, 13));
        System.out.println("Najdłyżej trwające wydarzenie: ");
        print(findLongest(records));

    }

    public static void print(Record[] records) {
        for (Record record : records) {
            print(record);
        }
    }

    public static void printBefore(Record[] records, LocalDate date) {
        for (Record record : records) {
            if (record.getStart().isBefore(date.atStartOfDay())) {
                print(record);
            }
        }
    }

    public static void print(Record record) {
        System.out.println(record.getName() + " " + record.getStart() + " " + record.getEnd());
    }

    public static Record findLongest(Record[] records) {
        Record record = records[0];
        Duration duration = Duration.between(record.getStart(), record.getEnd());

        for (int i = 1; i < records.length; i++) {
            Record other = records[i];
            Duration otherDuration = Duration.between(other.getStart(), other.getEnd());
            if (duration.compareTo(otherDuration) < 0) {
                record = other;
                duration = otherDuration;
            }
        }
        return record;
    }
}
