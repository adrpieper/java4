package innerclasse;

/**
 * Created by Adrian on 2017-08-17.
 */
public class Car {

    private boolean open = false;
    private Door door = new Door();

    public Door getDoor() {
        return door;
    }

    public boolean isOpen() {
        return open;
    }

    public class Door {
        public void open() {
            open = true;
        }

        public void close() {
            open = false;
        }
    }
}
