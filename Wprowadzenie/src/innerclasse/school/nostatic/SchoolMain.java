package innerclasse.school.nostatic;

/**
 * Created by Adrian on 2017-08-17.
 */
public class SchoolMain {

    public static void main(String[] args) {
        School school = new School("III LO", "Kazimierza Wielkiego");
        School school1 = new School("II LO", "Kazimierza Małego");
        School.Pupil pupil0 = school.newPupil("Jan");
        School.Pupil pupil1 = school.newPupil("Adam");
        School.Pupil pupil2 = school1.newPupil("Adam");
        pupil0.introduce();
        pupil1.introduce();
        pupil2.introduce();
    }
}
