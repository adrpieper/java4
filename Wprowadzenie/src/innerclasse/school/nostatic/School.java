package innerclasse.school.nostatic;

/**
 * Created by Adrian on 2017-08-17.
 */
public class School {
    private String name;
    private String patron;

    public School(String name, String patron) {
        this.name = name;
        this.patron = patron;
    }

    public Pupil newPupil(String name) {
        return new Pupil(name);
    }

    public String getName() {
        return name;
    }

    public class Pupil {
        private String name;

        public Pupil(String name) {
            this.name = name;
        }

        public void introduce() {
            System.out.println("Nazywam się " + name + ". " +
                    "Uczęszczam do szkoły " + School.this.name +
                    " imienia " + patron);
        }

        public String getName() {
            return name;
        }
    }
}
