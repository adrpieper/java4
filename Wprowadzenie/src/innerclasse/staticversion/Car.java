package innerclasse.staticversion;

/**
 * Created by Adrian on 2017-08-17.
 */
public class Car {

    private boolean open = false;
    private Door door = new Door(this);

    public Door getDoor() {
        return door;
    }
    public boolean isOpen() {
        return open;
    }

    public static class Door {
        private Car car;

        public Door(Car car) {
            this.car = car;
        }

        public void open() {
            car.open = true;
        }

        public void close() {
            car.open = false;
        }
    }
}
