package innerclasse.staticversion;

/**
 * Created by Adrian on 2017-08-17.
 */
public class CarMain {

    public static void main(String[] args) {
        Car car = new Car();

        System.out.println(car.isOpen());
        Car.Door door = car.getDoor();
        door.open();
        System.out.println(car.isOpen());


    }
}
