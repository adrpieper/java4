package innerclasse.village.nostatic;

/**
 * Created by Adrian on 2017-08-17.
 */
public class VillageMain {

    public static void main(String[] args) {
        Village village = new Village();
        village.getFarmer().work();
        village.getFarmer().work();
        village.getFarmer().work();
        village.getFarmer().work();
        System.out.println(village.getFood());
    }
}
