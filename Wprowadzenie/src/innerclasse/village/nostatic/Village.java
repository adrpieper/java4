package innerclasse.village.nostatic;

/**
 * Created by Adrian on 2017-08-17.
 */
public class Village {
    private int food;
    private Farmer farmer = new Farmer();

    public Farmer getFarmer() {
        return farmer;
    }

    public int getFood() {
        return food;
    }

    public class Farmer {

        public void work() {
            food += 10;
        }
    }
}
