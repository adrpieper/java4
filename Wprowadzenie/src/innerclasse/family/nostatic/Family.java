package innerclasse.family.nostatic;

/**
 * Created by Adrian on 2017-08-17.
 */
public class Family {
    private Mother mother;
    private Father father;

    public Family(String matherName, String fatherName) {
        mother = new Mother(matherName);
        father = new Father(fatherName);
    }

    public Father getFather() {
        return father;
    }

    public Mother getMother() {
        return mother;
    }

    class Mother {
        private String name;

        Mother(String name) {
            this.name = name;
        }

        public void introduce() {
            System.out.println("Nazywam się "+name+", mój mąż to "+father.name);
        }
    }

    class Father {
        private String name;

        Father(String name) {
            this.name = name;
        }

        public void introduce() {
            System.out.println("Nazywam się "+name+", mója żona to "+mother.name);
        }
    }
}
