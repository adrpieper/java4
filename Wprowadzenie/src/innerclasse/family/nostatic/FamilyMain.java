package innerclasse.family.nostatic;

/**
 * Created by Adrian on 2017-08-17.
 */
public class FamilyMain {

    public static void main(String[] args) {
        Family family = new Family("Grażyna", "Janusz");
        family.getFather().introduce();
        family.getMother().introduce();
    }
}
