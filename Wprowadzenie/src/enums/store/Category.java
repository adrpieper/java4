package enums.store;

/**
 * Created by Adrian on 2017-08-16.
 */
public enum Category {
    FOOD,
    CLOTHES,
    ALCOHOL,
    TABACCO;

}
