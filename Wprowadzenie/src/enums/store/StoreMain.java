package enums.store;

import java.util.List;

/**
 * Created by Adrian on 2017-08-16.
 */
public class StoreMain {

    public static void main(String[] args) {
        Store store = new Store();
        store.add("Masło", 6 , Category.FOOD);
        store.add("Chleb", 2, Category.FOOD);
        store.add("Piwo", 3, Category.ALCOHOL);

        Product product = store.get("Masło");
        System.out.println("Masło : ");
        System.out.println(product);

        System.out.println("Jedzenie : ");
        List<Product> food = store.getAll(Category.FOOD);
        for (Product p : food) {
            System.out.println(p);
        }
        store.remove("Masło");
        System.out.println("Po usunięciu masła : ");
        for (Product p : store.getAll(Category.FOOD)) {
            System.out.println(p);
        }
    }
}
