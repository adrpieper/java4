package enums.order;

import java.time.LocalDate;

/**
 * Created by Adrian on 2017-08-16.
 */
public class Order {
    private Status status;
    private LocalDate date;

    public Order(Status status, LocalDate date) {
        this.status = status;
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    public Status getStatus() {
        return status;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Order{" +
                "status=" + status +
                ", date=" + date +
                '}';
    }
}
