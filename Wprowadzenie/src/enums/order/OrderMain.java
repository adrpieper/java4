package enums.order;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 2017-08-16.
 */
public class OrderMain {

    public static void main(String[] args) {

        List<Order> orders = createOrders();
        printAll(orders);
        updateOrders(orders);
        printAll(orders);
    }

    private static void updateOrders(List<Order> orders) {
        LocalDate sevenDaysAgo = LocalDate.now().minusWeeks(1);

        for (Order order : orders) {
            if (order.getDate().isBefore(sevenDaysAgo)) {
                order.setStatus(Status.OLD);
            }
        }
    }

    private static List<Order> createOrders() {
            List<Order> orders = new ArrayList<>();
            orders.add(new Order(Status.NEW, LocalDate.of(2017,8,15)));
            orders.add(new Order(Status.NEW, LocalDate.of(2017,8,12)));
            orders.add(new Order(Status.NEW, LocalDate.of(2017,8,1)));
            orders.add(new Order(Status.NEW, LocalDate.of(2017,7,12)));
            orders.add(new Order(Status.NEW, LocalDate.of(2017,7,12)));
            return orders;
    }

    private static void printAll(List<Order> orders) {
        for (Order o : orders) {
            System.out.println(o);
        }
    }
}
