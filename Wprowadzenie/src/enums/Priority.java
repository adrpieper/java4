package enums;

/**
 * Created by Adrian on 2017-08-16.
 */
public enum Priority {
    LOW, MEDIUM, HIGH;
}
