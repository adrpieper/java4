package enums;

/**
 * Created by Adrian on 2017-08-16.
 */
public class PriorityExample {

    public static void main(String[] args) {
        Priority priority = Priority.HIGH;
        priority = Priority.MEDIUM;
        System.out.println(priority.ordinal());
        System.out.println(priority.name());

        System.out.println(priority);

        Priority[] priorities = Priority.values();
        for (Priority p : priorities) {
            System.out.println(p);
        }

        Priority low = Priority.valueOf("LOW");
        System.out.println(low == Priority.LOW);

        try {
            Priority medium = Priority.valueOf("m");
        }catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(Priority.LOW.compareTo(Priority.HIGH));
    }
}
