package casting;

/**
 * Created by Adrian on 2017-08-17.
 */
public class Examples {

    public static void main(String[] args) {
        int i = 100;
        long l = i;
        i = (int) l;
        int max = Integer.MAX_VALUE;
        System.out.println(max);
        float f = max;
        System.out.println(f);
        Object o = 2;
        Integer integer = (Integer) o;
        Number number = integer;
        Boolean bool = (Boolean) o;
        System.out.println(integer.intValue());
    }
}
