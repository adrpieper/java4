package opp.person.good;

/**
 * Created by Adrian on 2017-08-02.
 */
public class Osoba {
    private String imie;
    private int wiek;

    //Konstruktor
    public Osoba(String imie, int wiek) {
        // Instrukcja this odnosi się do nowo tworzonej osoby
        this.imie = imie;
        this.wiek = wiek;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }
}
