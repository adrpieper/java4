package opp.person.good;

/**
 * Created by Adrian on 2017-08-02.
 */
public class OsobaMain {

    public static void main(String[] args) {
        Osoba jan = new Osoba("Jan", 20);
        Osoba adam = new Osoba("Adam", 24);
        Osoba ania = new Osoba("Ania", 18);

        jan.setWiek(30);

        Osoba[] osoby = {jan, adam, ania};

        for (Osoba osoba : osoby) {
            System.out.println(osoba.getWiek());
            System.out.println(osoba.getImie());
        }
    }
}
